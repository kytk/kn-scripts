#!/usr/bin/env bash
# genroi.sh
# generate rois from atlas
# Usage: genroi.sh <atlas> 
# The atlas_rois directory will be created and 
# the output roi files would be atlas_001.nii.gz, atlas_002.nii.gz... 
# under atlas_rois directory
# Prerequisite: This script uses FSL
# K.Nemoto 06 Oct 2022 

# For debug, please uncomment the following line
#set -x

if [ $# -ne 1 ] ; then
  echo "Please specify the atlas you want to extract ROIs and output basename!"
  echo "Usage: $0 <atlas>"
  exit 1
fi

atlas=$(imglob $1)

# output directory
mkdir ${atlas}_rois
outdir=${atlas}_rois

# Check how many regions the atlas has
numroi=$(fslstats $atlas -R | awk '{ print int($2) }')

for f in $(seq -w $numroi)
do 
  lthr=$(echo "$f - 0.5" | bc)
  uthr=$(echo "$f + 0.5" | bc)
  fslmaths $atlas -thr $lthr -uthr $uthr -bin ${outdir}/${atlas}_${f}
  signal=$(fslstats ${outdir}/${atlas}_${f} -M | cut -c 1)
  [[ $signal == 0 ]] && rm ${outdir}/${atlas}_${f}.nii.gz  
done

echo "Done."

exit

