#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Script to remove trigger time from Philips fMRI
# source: https://github.com/rordenlab/dcm2niix/issues/428
# 14 Oct 2023 K. Nemoto

import sys, os, time, argparse
import pydicom

__version__ = '20231004'

__desc__ = '''
Remove Trigger Time (0018,1060) from Philips rsfMRI
'''
__epilog__ = '''
examples:
  dcm_rm_trigger_time.py DICOM_DIR1 DICOM_DIR2 ...
'''

def remove_triggertime(src_dir):
    # modify files
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            try:
                src_file = os.path.join(root, file)
                ds = pydicom.dcmread(src_file)
                pid = src_dir.replace('/','')
                del ds[0x0018, 0x1060]
                ds.save_as(src_file)
            except:
                pass

if __name__ == '__main__':
    start_time = time.time()
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('dirs', metavar='DICOM_DIR', help='DICOM directories.', nargs='+')

    err = 0
    try:
        args = parser.parse_args()
        for dicom_dir in args.dirs:  # Loop through all the provided directories
            print(f'remove dicom tag (0018,1060) from {dicom_dir}')
            remove_triggertime(dicom_dir)
        print("execution time: %.2f second." % (time.time() - start_time))
    except Exception as e:
        print("%s: error: %s" % (__file__, str(e)))
        err = 1

    sys.exit(err)
