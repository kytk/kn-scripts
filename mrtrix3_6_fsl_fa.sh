#!/usr/bin/env bash

[[ -d dti ]] || mkdir dti

for f in "$@"

do
 if [[ "$f" == *_den_unr_preproc_unbiased.mif ]]; then
    dwi=${f%_den_unr_preproc_unbiased.mif}
    mrconvert $f ${dwi}_unbiased.nii.gz \
      -export_grad_fsl ${dwi}_unbiased.bvec ${dwi}_unbiased.bval 

    dtifit -k ${dwi}_unbiased.nii.gz \
           -o dti/${dwi} \
           -m ${dwi}_mask.nii.gz \
           -r ${dwi}_unbiased.bvec \
           -b ${dwi}_unbiased.bval
           
  else
    echo "ERROR: Please specify *_den_unr_preproc_unbiased.mif files!"
    exit 1
 fi

  
done


