#!/usr/bin/env bash

#引数にCOCOROIDのディレクトリを指定することを想定
for dir in "$@"
do
  #find でファイルを見つけて、ソートして、
  #最初のファイルを fname という変数にいれる
  fname=$(find $dir -type f | sort | head -n 1)
  #ファイールド数という awk の特殊な変数 $NF=最後のフィールド  
  new_uid=$(dcm_print_header.py $fname |\
   grep '0020, 000d' | awk '{ print $NF }')
  #変数 dir に対して、 変数 new_uid を書き込む
  dcm_modify_studyinstanceuid.py $dir ${new_uid}
done

