#!/usr/bin/env python3

import os
import sys
import pydicom

def print_studyinstanceuid(directory):
    for root, dirs, files in os.walk(directory):
        for file in files:
            try:
                dicom_file_path = os.path.join(root, file)
                dicom_file = pydicom.dcmread(dicom_file_path, force=True)
                # DICOMファイルとして読み込み可能かどうかを確認
                if "StudyInstanceUID" in dicom_file:
                    study_instance_uid = dicom_file.StudyInstanceUID
                    print(f"{os.path.basename(root)}, {file}, {study_instance_uid}")
                else:
                    print(f"File does not appear to be a DICOM file: {file}")
            except Exception as e:
                print(f"Error reading {file}: {e}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: dcm_print_studyinstanceuid.py DICOM_DIR")
        sys.exit(1)
    
    dicom_dir = sys.argv[1]
    print_studyinstanceuid(dicom_dir)

