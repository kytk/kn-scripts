#!/usr/bin/env bash

# Script to calc regional FA based on JHU-ICBM-labels-1mm.nii.gz
# K.Nemoto 21 Dec 2023

roi=${FSLDIR}/data/atlases/JHU/JHU-ICBM-labels-1mm.nii.gz

# Labels
cat << EOS > tmp.labels
Region
Middle_cerebellar_peduncle
Pontine_crossing_tract
Genu_of_corpus_callosum
Body_of_corpus_callosum
Splenium_of_corpus_callosum
Fornix_(column_and_body_of_fornix)
Corticospinal_tract_R
Corticospinal_tract_L
Medial_lemniscus_R
Medial_lemniscus_L
Inferior_cerebellar_peduncle_R
Inferior_cerebellar_peduncle_L
Superior_cerebellar_peduncle_R
Superior_cerebellar_peduncle_L
Cerebral_peduncle_R
Cerebral_peduncle_L
Anterior_limb_of_internal_capsule_R
Anterior_limb_of_internal_capsule_L
Posterior_limb_of_internal_capsule_R
Posterior_limb_of_internal_capsule_L
Retrolenticular_part_of_internal_capsule_R
Retrolenticular_part_of_internal_capsule_L
Anterior_corona_radiata_R
Anterior_corona_radiata_L
Superior_corona_radiata_R
Superior_corona_radiata_L
Posterior_corona_radiata_R
Posterior_corona_radiata_L
Posterior_thalamic_radiation_R
Posterior_thalamic_radiation_L
Sagittal_stratum_R
Sagittal_stratum_L
External_capsule_R
External_capsule_L
Cingulum_(cingulate_gyrus)_R
Cingulum_(cingulate_gyrus)_L
Cingulum_(hippocampus)_R
Cingulum_(hippocampus)_L
Fornix_(cres)_Stria_terminalis_R
Fornix_(cres)_Stria_terminalis_L
Superior_longitudinal_fasciculus_R
Superior_longitudinal_fasciculus_L
Superior_fronto-occipital_fasciculus_R
Superior_fronto-occipital_fasciculus_L
Inferior_fronto-occipital_fasciculus_R
Inferior_fronto-occipital_fasciculus_L
Uncinate_fasciculus_R
Uncinate_fasciculus_L
Tapetum_R
Tapetum_L
EOS

# mean FA within ROI
for f in "$@"
do
  echo $f > tmp.$f
  fslstats -K $roi $f -M >> tmp.$f
done

paste tmp.labels tmp.*nii* > results_$(date +%Y%m%d_%H%M%S).tsv

rm tmp*
 

