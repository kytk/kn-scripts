#!/usr/bin/env bash

set -x

for f in "$@"
do
  if [[ "$f" == *mif ]]; then
    dwi=${f%.mif}
    dwidenoise $f ${dwi}_den.mif -noise ${dwi}_noise.mif
    mrdegibbs ${dwi}_den.mif ${dwi}_den_unr.mif -axes 0,1
  else
    echo "ERROR: Please specify mif files!"
 fi
done

