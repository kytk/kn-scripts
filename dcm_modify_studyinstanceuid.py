#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# A script to modify the "Study Instance UID" tag (0020,000D) in DICOM files

# 15 Feb 2024 K. Nemoto

import sys, os, time, argparse
import pydicom

__version__ = '20240215'

__desc__ = '''
This script modifies the StudyInstanceUID in DICOM files.
Please specify two arguments: "DICOM directory" and "New Study Instance UID"
'''
__epilog__ = '''
examples:
  script.py DICOM_DIR NEW_STUDY_INSTANCE_UID 
'''

def modify_study_instance_uid(src_dir, new_study_instance_uid):
    # Modify files
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            try:
                src_file = os.path.join(root, file)
                ds = pydicom.dcmread(src_file)
                ds.StudyInstanceUID = new_study_instance_uid  # Correct way to modify StudyInstanceUID
                ds.save_as(src_file)
            except Exception as e:
                print(f"Failed to modify {src_file}: {e}")

if __name__ == '__main__':
    start_time = time.time()
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('DICOM_DIR', help='DICOM directory', nargs=1)
    parser.add_argument('NEW_STUDY_INSTANCE_UID', help='New Study Instance UID', nargs=1)

    args = parser.parse_args()
    print(f"Modifying Study Instance UID to {args.NEW_STUDY_INSTANCE_UID[0]} in {args.DICOM_DIR[0]}")
    modify_study_instance_uid(args.DICOM_DIR[0], args.NEW_STUDY_INSTANCE_UID[0])
    print(f"Execution time: {time.time() - start_time:.2f} seconds.")

