#!/usr/bin/env bash

for f in "$@"

do
 if [[ "$f" == *den_unr_preproc.mif ]]; then
    dwi=${f%.mif}
    dwibiascorrect ants ${dwi}.mif ${dwi}_unbiased.mif -bias ${dwi}_bias.mif
  else
    echo "ERROR: Please specify *den_unr_preproc.mif files!"
 fi
done

