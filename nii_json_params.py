#!/usr/bin/env python3

# Script to print summary of json files
# 07 Jan 2025 K. Nemoto

import json
import sys
import os

def extract_mri_parameters(json_file):
    print(f"Reading file: {json_file}\n")
    
    # Check if file exists
    if not os.path.exists(json_file):
        print(f"Error: File '{json_file}' does not exist")
        sys.exit(1)
    
    # Load and validate JSON file
    try:
        with open(json_file, 'r') as f:
            data = json.load(f)
    except json.JSONDecodeError as e:
        print(f"Error: Invalid JSON format in '{json_file}'")
        print(f"Details: {str(e)}")
        sys.exit(1)
    except Exception as e:
        print(f"Error: Unable to read file '{json_file}'")
        print(f"Details: {str(e)}")
        sys.exit(1)
    
    # Extract specified parameters
    parameters = {
        'Manufacturer': data.get('Manufacturer'),
        'ManufacturersModelName': data.get('ManufacturersModelName'),
        'SliceThickness': data.get('SliceThickness'),
        'EchoTime': data.get('EchoTime'),
        'RepetitionTime': data.get('RepetitionTime'),
        'EffectiveEchoSpacing': data.get('EffectiveEchoSpacing'),
        'TotalReadoutTime': data.get('TotalReadoutTime'),
        'PhaseEncodingDirection': data.get('PhaseEncodingDirection'),
        'BidsGuess': data.get('BidsGuess')
    }
    
    # Display results
    for key, value in parameters.items():
        print(f"{key}: {value}")

# Check if a JSON file is provided as an argument
if len(sys.argv) != 2:
    print("Usage: python nii_json_params.py <json_file>")
    sys.exit(1)

# Execute the program with the provided JSON file
json_file = sys.argv[1]
extract_mri_parameters(json_file)
