#!/usr/bin/env bash

for dir in "$@"
do
  base=${dir%/}
  tar -cvf ${base}.tar ${base}
done

