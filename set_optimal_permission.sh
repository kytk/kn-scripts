#!/usr/bin/env bash

# script to change permission of files to 644 and direcotries to 755

# K. Nemoto 8 Apr 2024

workingDir=$1

find ${workingDir} -type d -exec chmod 755 {} \;
find ${workingDir} -type f -exec chmod 644 {} \;

