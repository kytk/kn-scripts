#!/bin/sh
#Script to generate proportional scaled image, which might be useful
# for PET/SPECT images
#K. Nemoto 16 July 2014

#Set the output type of file image to NIFTI (.nii)
FSLOUTPUTTYPE=NIFTI

#Check input image(s) are specified
if [ $# -lt 1 ]; then
    echo "Please specify the images!"
    echo "Usage: $0 images"
    exit 1
fi

#Generate proportional scaled images
for f in $@; do

    #Step1: calculate mean of an image
    firstmean=`fslstats $f -m`

    #Step2: calculate the threshold by firstmean / 8
    thresh=`echo "$firstmean / 8" | bc -l`

    #Step3: calculate global value by averaging the voxel values
    # above the threshold
    glob=`fslstats $f -l $thresh -m`

    #For Debug: Display the global value
    echo "Global of $f = $glob"

    #Step4: Set the global value of an image to 50 with proportional scaling
    fslmaths $f -mul 50 -div $glob p$f

    #For Debug: Display the global value of proportional scaled image
    #Value should be 50
    pfirstmean=`fslstats p$f -m`
    pthresh=`echo "$pfirstmean / 8" | bc -l`
    pglob=`fslstats p$f -l $pthresh -m`
    echo "Global of p$f = $pglob"

done
