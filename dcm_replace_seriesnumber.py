#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# A script to replace "Series Number" tag (0020,0011)
# Part of this script is based on the script provided by Yuya Saito

# 21 Feb 2023 K. Nemoto
 
import sys, os, time, argparse
import pydicom
 
__version__ = '20230221'
 
__desc__ = '''
Replace Series Number
Please specify two arguments; "DICOM directory" and "Series Number"
'''
__epilog__ = '''
examples:
  dcm_replace_seriesnumber.py DICOM_DIR SERIES_NUMBER 
'''

def replace_series_number(src_dir, series_number):
    # modify files
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            try:
                src_file = os.path.join(root, file)
                ds = pydicom.dcmread(src_file)
                ds.add_new([0x20,0x11], 'IS', series_number)
                #print('Add Series Description: {} in {}'.format(series_descrip,src_file))
                ds.save_as(src_file)
            except:
                pass


if __name__ == '__main__':
    start_time = time.time()
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('DICOM_DIR', help='DICOM directory', nargs=1)
    parser.add_argument('SERIES_NUMBER', help='Series Number', nargs=1)
 
    err = 0
    try:
        args = parser.parse_args()
        print("Add {} as Series Number in {}".format(args.SERIES_NUMBER[0], args.DICOM_DIR[0]))
        replace_series_number(args.DICOM_DIR[0], args.SERIES_NUMBER[0])
        print("execution time: %.2f second." % (time.time() - start_time))
    except Exception as e:
        print("%s: error: %s" % (__file__, str(e)))
        err = 1
 
    sys.exit(err)
