#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
script to identify TE(s) in the set of DICOM files

Assumption: DICOM files are already sorted and stored under each series
e.g.

sub-01
├── 02_MPRAGE
│   ├── IM000001.dcm
│   └── IM000002.dcm
├── 03_ep2d_DTI_b1000_30dir_av1_81
│   ├── IM000001.dcm
│   └── IM000002.dcm
├── 04_field_mapping_mag+pha
│   ├── IM000001.dcm
│   └── IM000002.dcm
├── 05_field_mapping_mag+pha
│   ├── IM000001.dcm
│   └── IM000002.dcm
└── 06_resting-state_fMRI
    ├── IM000001.dcm
    └── IM000002.dcm

Usage: just run this script under sub-01 in the example above.
       The script returns TE(s) for each directory.

    TE in 02_MPRAGE: {2.52}
    TE in 03_ep2d_DTI_b1000_30dir_av1_81: {81.0}
    TE in 04_field_mapping_mag+pha: {4.92, 7.38}
    TE in 05_field_mapping_mag+pha: {7.38}
    TE in 06_resting-state_fMRI: {30.0}

@author: K. Nemoto 02 Jan 2023
"""

import os, pydicom

dirlist = os.listdir()
dirlist.sort()

for dir in dirlist:
    os.chdir(str(dir))
    telist = []
    for dcm in os.listdir():
        ds = pydicom.dcmread(dcm)
        echotime = float(ds.EchoTime)
        telist.append(echotime)
        te = set(telist)

    print('TE in {}: {}'.format(dir, te))
    os.chdir('../')
    
    