#!/usr/bin/env python3

"""
Script to print DICOM header or summary
Usage: dcm_print_header.py [-h] [-s] [-v] file [file ...]
   or: dcm_print_summary.py [-h] [-v] file [file ...]

Authors: K.Nemoto
"""

__version__ = '20250102'

from pydicom import dcmread
import argparse
import sys
import os

def print_summary(ds, file_path):
    """Print summary of DICOM header"""
    filename = os.path.basename(file_path)
    
    # Define the fields to be included in the summary
    fields = [
        "PatientID",
        "PatientName",
        "StudyDate",
        "InstitutionName",
        "Manufacturer",
        "ManufacturerModelName",
        "MagneticFieldStrength",
        "DeviceSerialNumber",
        "SeriesNumber",
        "ProtocolName",
        "SeriesDescription",
        "EchoTime",
        "RepetitionTime",
        "FlipAngle",
        "AcquisitionMatrix",
        "PixelSpacing",
        "SliceThickness",
	"ImageType"
    ]

    print(f"\nDICOM Summary of {filename}:")
    print("-" * 50)
    for field in fields:
        if field in ds:
            print(f"{field}: {ds.data_element(field).value}")
        else:
            print(f"{field}: Not available")
    print("-" * 50)

def print_header(ds, file_path):
    """Print full DICOM header"""
    filename = os.path.basename(file_path)
    print(f"\nDICOM Header of {filename}:")
    print("-" * 50)
    print(ds)
    print("-" * 50)

def process_dicom_file(file_path, summary_mode=False):
    """Process a single DICOM file"""
    try:
        # Read the DICOM file
        ds = dcmread(file_path)
        
        # Print either summary or full header based on mode
        if summary_mode:
            print_summary(ds, file_path)
        else:
            print_header(ds, file_path)
            
    except PermissionError:
        print(f"Error: Permission denied accessing file '{file_path}'")
    except IsADirectoryError:
        print(f"Error: '{file_path}' is a directory, not a file")
    except Exception as e:
        print(f"Error reading the DICOM file '{file_path}': {e}")

def main():
    # Get the script name
    script_name = os.path.basename(sys.argv[0])
    
    # Check if running as summary script
    is_summary_mode = script_name == 'dcm_print_summary.py'
    
    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Display DICOM header information or summary'
    )
    parser.add_argument(
        'files', 
        nargs='*',
        help='One or more DICOM files'
    )
    
    # Only show -s option when running as dcm_print_header.py
    if not is_summary_mode:
        parser.add_argument(
            '-s', '--summary', 
            action='store_true',
            help='Print summary instead of full header'
        )
    
    parser.add_argument(
        '-v', '--version',
        action='version',
        version=f'%(prog)s {__version__}',
        help='Show program version'
    )
    
    args = parser.parse_args()

    # If no files are specified and no version is requested, show help
    if not args.files:
        parser.print_help()
        sys.exit(1)

    # Set summary mode based on script name or -s option
    summary_mode = is_summary_mode or (hasattr(args, 'summary') and args.summary)

    # Process each file
    for file_path in args.files:
        if not os.path.exists(file_path):
            print(f"Error: The file '{file_path}' does not exist.")
            continue
            
        if not os.path.isfile(file_path):
            print(f"Error: '{file_path}' is not a file.")
            continue
            
        process_dicom_file(file_path, summary_mode)

if __name__ == "__main__":
    main()
