#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# A script to fill PatientID with the name of source directory
# Part of this script is based on the script provided by Yuya Saito

# 05 Sep 2024 K. Nemoto
 
import sys, os, time, re, shutil, argparse, subprocess
import pydicom
 
__version__ = '20240905'
 
__desc__ = '''
Fill other PatientID with the name of the input directories
'''
__epilog__ = '''
examples:
  dcm_fill_anotherid.py DICOM_DIR1 DICOM_DIR2 DICOM_DIR3
'''

def fill_id(src_dir):
    # modify files
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            try:
                src_file = os.path.join(root, file)
                ds = pydicom.dcmread(src_file)
                pid = os.path.basename(src_dir)
                ds.add_new([0x10,0x1001], 'PN', pid)
                ds.save_as(src_file)
            except:
                pass


if __name__ == '__main__':
    start_time = time.time()
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('dirs', metavar='DICOM_DIR', help='DICOM directories.', nargs='+')
 
    err = 0
    try:
        args = parser.parse_args()
        for dir in args.dirs:
            print(f'Fill another ID of {os.path.basename(dir)}')
            fill_id(dir)
        print(f"Total execution time: {time.time() - start_time:.2f} seconds.")
    except Exception as e:
        print(f"{__file__}: error: {str(e)}")
        err = 1
 
    sys.exit(err)