#!/usr/bin/env bash
# This script uses dcm_add_studydate.py and inserts StudyDate id to DICOM files.

# Preparation
# Please prepare "subjlist" and "studydate"
# subjlist must contain "path to the source directory" and "another id"

# Example
# id_subj1	20210123
# id_subj2	20220321

# Usage
# dcm_studydate_batch.sh <subjlist>

if [[ $# -lt 1 ]] ; then
  echo "Please specify the subject list you want to add anotehr ID!"
  echo "Usage: $0 <subjlist>"
  exit 1
fi

# convert CR+LF to CR
tmplist=$(mktemp)
nkf -wLu $1 > tmplist

cat tmplist | while read id studydate; do dcm_add_studydate.py $id $studydate; done

if [[ $? -eq 0 ]] ; then
  echo "Done"
else
  echo "ERROR"
fi

