#!/usr/bin/env bash

set -x

# Default PE direction
pe_dir="PA"

# Function to display usage
usage() {
    echo "Usage: $0 [-d PE_DIR] input1.nii [input2.nii ...]"
    echo "  -d PE_DIR    Specify the phase encoding direction (AP or PA, default: PA)"
    exit 1
}

# Parse command line options
while getopts "d:" opt; do
    case $opt in
        d)
            pe_dir=$OPTARG
            if [[ "$pe_dir" != "AP" && "$pe_dir" != "PA" ]]; then
                echo "Error: PE direction must be either AP or PA"
                usage
            fi
            ;;
        \?)
            usage
            ;;
    esac
done

# Shift the options so that $@ contains the input files
shift $((OPTIND-1))

# Check if there are input files
if [ $# -eq 0 ]; then
    echo "Error: No input files specified"
    usage
fi

# Function to check if file exists
file_exists() {
    if [ ! -f "$1" ]; then
        echo "Error: File $1 not found!"
        exit 1
    fi
}

# Create directory for DTI outputs
[[ -d dti ]] || mkdir dti

# Process each input file
for f in "$@"
do
    if [[ "$f" == *nii* ]]; then
        # Step 1: Convert nifti to mif
        dwi=$(imglob $f)
        file_exists "${dwi}.bvec"
        file_exists "${dwi}.bval"
        mrconvert $f "${dwi}.mif" -fslgrad "${dwi}.bvec" "${dwi}.bval" \
            -datatype float32
        
        # Step 2: Denoise and remove Gibbs ringing
        dwidenoise "${dwi}.mif" "${dwi}_den.mif" -noise "${dwi}_noise.mif"
        mrdegibbs "${dwi}_den.mif" "${dwi}_den_unr.mif" -axes 0,1
        
        # Step 3: Preprocess with FSL (using specified PE direction)
        dwifslpreproc "${dwi}_den_unr.mif" "${dwi}_den_unr_preproc.mif" \
            -pe_dir $pe_dir -rpe_none -eddy_options " --slm=linear"
        
        # Step 4: Perform bias field correction
        dwibiascorrect ants "${dwi}_den_unr_preproc.mif" \
            "${dwi}_den_unr_preproc_unbiased.mif" -bias "${dwi}_bias.mif"
        
        # Step 5: Create a brain mask
        dwi2mask "${dwi}_den_unr_preproc_unbiased.mif" "${dwi}_mask.nii.gz"
        
        # Step 6: Convert MIF to NIfTI for FSL compatibility
        mrconvert "${dwi}_den_unr_preproc_unbiased.mif" \
            "${dwi}_unbiased.nii.gz" \
            -export_grad_fsl "${dwi}_unbiased.bvec" "${dwi}_unbiased.bval"
        
        # Step 7: Run FSL's dtifit
        dtifit -k "${dwi}_unbiased.nii.gz" \
               -o "dti/${dwi}" \
               -m "${dwi}_mask.nii.gz" \
               -r "${dwi}_unbiased.bvec" \
               -b "${dwi}_unbiased.bval"
        
        echo "Processing complete for $f"
    else
        echo "Error: Please specify nifti files!"
        exit 1
    fi
done

echo "All processing steps completed successfully!"
