#!/usr/bin/env bash

for f in "$@"

do
 if [[ "$f" == *den_unr.mif ]]; then
    dwi=${f%.mif}
    dwifslpreproc $f ${dwi}_preproc.mif \
      -pe_dir PA -rpe_none -eddy_options " --slm=linear"
  else
    echo "ERROR: Please specify *den_unr.mif files!"
 fi
done

