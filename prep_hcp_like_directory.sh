#!/usr/bin/env bash

cd ~/projects/Pipelines_ExampleData

for id in "$@"
do
  mkdir -p ${id}/unprocessed/3T/{T1w_MPR1,T2w_SPC1}
done

