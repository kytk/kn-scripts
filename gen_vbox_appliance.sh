#!/usr/bin/env bash
#A script to compact and export VirtualBox applilance

# Usage:
# gen_vbox_appliance.sh Virtual_machine_name shared_folder_name
#
# Virtual_machine_name can be found by the command
# vboxmanage list hdds

# 08 Dec 2024 K. Nemoto

timestamp=$(date +%Y%m%d)
vm=$1
sfname=$2
ovaname=${1}-${timestamp}

if [ $# -lt 1 ] ; then
  echo "Please specify the Virtual machine name you want to export!"
  echo "Usage: $0 Virtual_machine_name [Shared_folder_name]"
  exit 1
fi

#Get UUID for Virtual machine
echo "extract UUID of the virtual machine"
uuid=$(vboxmanage list hdds | grep -4 ${vm} | head -1 | awk '{ print $2 }')
echo "UUID of $vm is $uuid"

#set CPU to 2, memory to 4096MB, and VRAM to 256MB
echo "set cpu of $vm to 2, memory to 4096, and VRAM to 256"
vboxmanage modifyvm $vm --cpus 2 --memory 4096 --vram 256

#remove shared folder
vboxmanage sharedfolder remove $vm --name=$sfname

#compact
echo "compact virtual machine $vm for export"
vboxmanage modifyhd $uuid --compact

#export
echo "export virtual machine as ${ovaname}.ova"
vboxmanage export $vm --output ${ovaname}.ova --ovf20 --vsys 0 --version 1.$timestamp

#change permission
echo "change permission of ${ovaname}.ova"
chmod 644 ${ovaname}.ova

#generate md5
echo "generate md5 of ${ovaname}.ova as ${ovaname}.ova.md5"
openssl md5 ${ovaname}.ova > ${ovaname}.ova.md5

if [[ $? -eq 0 ]]; then
  echo "Finished!"
  exit
else
  echo "ERROR"
  exit 1
fi
