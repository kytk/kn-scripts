#!/usr/bin/env bash

for dir in "$@"
do
  base=${dir%/}
  zip -r ${base}.zip ${base}
done

