#!/usr/bin/env python3

import os
import sys
import pydicom
from collections import defaultdict

def find_duplicate_dicom_files(directory):
    # SOPInstanceUIDをキーとし、ファイルパスのリストを値とする辞書を初期化
    uid_to_files = defaultdict(list)
    
    # 指定されたディレクトリ内の全ファイルをスキャン
    for root, dirs, files in os.walk(directory):
        for file in files:
            try:
                # ファイルがDICOMファイルか確認
                filepath = os.path.join(root, file)
                dicom = pydicom.dcmread(filepath, stop_before_pixels=True)
                
                # SOPInstanceUIDを読み取り、辞書に追加
                sop_instance_uid = dicom.SOPInstanceUID
                uid_to_files[sop_instance_uid].append(filepath)
                
            except Exception as e:
                # DICOMファイルではない、または読み取り不可の場合はスキップ
                print(f"Error reading {file}: {e}")

    # 重複するSOPInstanceUIDを持つファイルを出力
    for uid, files in uid_to_files.items():
        if len(files) > 1:
            print(f"SOPInstanceUID: {uid} has the following duplicate files:")
            for file in files:
                print(file)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python find_duplicate_dicom_files.py <DICOMDIR>")
        sys.exit(1)

    dicom_dir = sys.argv[1]
    find_duplicate_dicom_files(dicom_dir)

