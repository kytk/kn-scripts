#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# A script to add "Study Descriptionin" tag (0008,103e)
# Part of this script is based on the script provided by Yuya Saito

# 01 Jan 2023 K. Nemoto
 
import sys, os, time, argparse
import pydicom
 
__version__ = '1.0 (2023/01/01)'
 
__desc__ = '''
Add Study Description
Please specify two arguments; "DICOM directory" and "Study Description"
'''
__epilog__ = '''
examples:
  dcm_add_studydescrip.py DICOM_DIR STUDY_DESCRIPTION 
'''

def add_study_descrip(src_dir, study_descrip):
    # modify files
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            try:
                src_file = os.path.join(root, file)
                ds = pydicom.dcmread(src_file)
                ds.add_new([0x08,0x1030], 'LO', study_descrip)
                #print('Add Study Description: {} in {}'.format(study_descrip,src_file))
                ds.save_as(src_file)
            except:
                pass


if __name__ == '__main__':
    start_time = time.time()
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('DICOM_DIR', help='DICOM directory', nargs=1)
    parser.add_argument('STUDY_DESCRIP', help='Study Description', nargs=1)
 
    err = 0
    try:
        args = parser.parse_args()
        print("Add {} as Study Description in {}".format(args.STUDY_DESCRIP[0], args.DICOM_DIR[0]))
        add_study_descrip(args.DICOM_DIR[0], args.STUDY_DESCRIP[0])
        print("execution time: %.2f second." % (time.time() - start_time))
    except Exception as e:
        print("%s: error: %s" % (__file__, str(e)))
        err = 1
 
    sys.exit(err)
