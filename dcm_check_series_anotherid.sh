#!/usr/bin/env bash

# Script to check SeriesNumber, SeriesDescription, and OtherPatientNames 
# in DICOM files

# 6 Apr 2023 K. Nemoto

for dir in $(ls -F | grep /)
do 
  echo $dir
  dicom=$(find $dir -type f | sort | tail -n 1)
  echo "  SeriesNumber: $(dcmdump $dicom 2> /dev/null | grep SeriesNumber | sed -e 's/^.*\[//' -e 's/\].*$//')"
  echo "  SeriesDescription: $(dcmdump $dicom 2> /dev/null | grep SeriesDescription | sed -e 's/^.*\[//' -e 's/\].*$//')"
  echo "  OtherPatientNames: $(dcmdump $dicom 2> /dev/null | grep OtherPatient | sed -e 's/^.*\[//' -e 's/\].*$//')"
done

