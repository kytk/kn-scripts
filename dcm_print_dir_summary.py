#!/usr/bin/env python3

# Script to print a DICOM header summary from a specified DICOM directory
# Usage: dcm_print_dir_summary.py <DICOM directory>
# Prerequisite: pydicom
# K.Nemoto 21 Mar 2024

from pydicom import dcmread
from pydicom.errors import InvalidDicomError
import sys
import os

def is_dicom_file(filepath):
    try:
        # Only attempt to read the metadata to improve performance
        dcmread(filepath, stop_before_pixels=True)
        return True
    except InvalidDicomError:
        return False

def find_dicom_files(directory):
    dicom_files = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            filepath = os.path.join(root, file)
            if is_dicom_file(filepath):
                dicom_files.append(filepath)
    return dicom_files

def main():
    args = sys.argv

    if len(args) < 2:
        print("Please specify a DICOM directory! Usage: dcm_summary.py <DICOM directory>")
        sys.exit(1)

    dir_path = args[1]

    if not os.path.isdir(dir_path):
        print(f"Error: The directory '{dir_path}' does not exist.")
        sys.exit(1)

    dicom_files = find_dicom_files(dir_path)
    file_count = len(dicom_files)

    if file_count == 0:
        print("No DICOM files found in the directory.")
        sys.exit(1)

    # For simplicity, taking the last DICOM file in the directory
    # because the first DICOM sometimes does not include image
    first_dicom_file = dicom_files[-1]

    try:
        ds = dcmread(first_dicom_file)
        print(f"Total DICOM files in directory: {file_count}")
        print_summary(ds, first_dicom_file)
    except Exception as e:
        print(f"Error reading the DICOM file: {e}")
        sys.exit(1)

def print_summary(ds, file_path):
    # Extract the filename from the file path
    filename = os.path.basename(file_path)

    # Define the fields to be included in the summary
    fields = [
        "PatientID",
        "PatientName",
        "StudyDate",
        "InstitutionName",
        "Manufacturer",
        "ManufacturerModelName",
        "MagneticFieldStrength",
        "DeviceSerialNumber",
        "SeriesNumber",
        "ProtocolName",
        "SeriesDescription",
        "EchoTime",
        "RepetitionTime",
        "FlipAngle",
        "AcquisitionMatrix",
        "PixelSpacing",
        "SliceThickness",
        "PixelBandwidth"
    ]

    print(f"DICOM Summary of {filename}:")
    for field in fields:
        if field in ds:
            print(f"{field}: {ds.data_element(field).value}")
        else:
            print(f"{field}: Not available")


if __name__ == "__main__":
    main()

