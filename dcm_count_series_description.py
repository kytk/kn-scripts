#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Script to count files of the same Series Description and Series Number
# Prerequisite: Pydicom

# K. Nemoto 17 Oct 2023


import os
import sys
import pydicom

def is_dicom_file(filepath):
    """Check if the given file is a DICOM file."""
    try:
        pydicom.dcmread(filepath)
        return True
    except:
        return False

def count_series_description_and_number_in_dir(directory):
    """Count SeriesDescription and SeriesNumber in a specified directory."""
    # Initialize a dictionary for SeriesDescription and SeriesNumber counts
    series_info_count = {}

    # Check all files in the directory
    for filename in os.listdir(directory):
        filepath = os.path.join(directory, filename)

        # Check if the file is a DICOM file
        if os.path.isfile(filepath) and is_dicom_file(filepath):
            dicom_file = pydicom.dcmread(filepath)

            # Check if the SeriesDescription and SeriesNumber tags exist
            if "SeriesDescription" in dicom_file and "SeriesNumber" in dicom_file:
                series_desc = dicom_file.SeriesDescription
                series_num = str(dicom_file.SeriesNumber)  # Convert SeriesNumber to string for consistency
                series_info = (series_desc, series_num)

                # Update the count for this SeriesDescription and SeriesNumber combination
                if series_info in series_info_count:
                    series_info_count[series_info] += 1
                else:
                    series_info_count[series_info] = 1

    return series_info_count

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 dcm_count_series_description.py <DICOM_DIR>")
        sys.exit(1)

    root_dir = sys.argv[1]
    if not os.path.isdir(root_dir):
        print(f"{root_dir} is not a valid directory.")
        sys.exit(1)

    # Count for the specified directory and all its subdirectories
    for dirpath, dirnames, filenames in os.walk(root_dir):
        results = count_series_description_and_number_in_dir(dirpath)
        if results:  # Only output if there are results
            print(f"Directory: {dirpath}")
            for (desc, num), count in results.items():
                print(f"SeriesDescription: {desc}, SeriesNumber: {num} => Count: {count}")
            print('----')  # Separator for each directory's results

