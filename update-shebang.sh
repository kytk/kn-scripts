#!/usr/bin/env bash

# Script to replace shebang line "#!/bin/bash" as "#!/usr/bin/env bash"
# K. Nemoto 12 Feb 2025

# Function to display usage information
show_usage() {
    echo "Usage: $0 [check|update] directory"
    echo "  check   : Display files that need to be modified"
    echo "  update  : Perform the actual file updates"
    echo "  directory : Target directory to process"
    exit 1
}

# Check command line arguments
if [ $# -ne 2 ]; then
    show_usage
fi

action=$1
directory=$2

# Check if directory exists
if [ ! -d "$directory" ]; then
    echo "Error: Directory '$directory' not found."
    exit 1
fi

# Find files that need to be updated
find_target_files() {
    find "$directory" -type f -name "*.sh" -exec grep -l "^#!/bin/bash" {} \;
}

case "$action" in
    "check")
        echo "The following files will be modified:"
        files=$(find_target_files)
        if [ -z "$files" ]; then
            echo "No files need to be updated."
            exit 0
        fi
        echo "$files"
        ;;
    "update")
        files=$(find_target_files)
        if [ -z "$files" ]; then
            echo "No files need to be updated."
            exit 0
        fi
        
        echo "The following files will be updated:"
        echo "$files"
        echo "Do you want to continue? (y/N)"
        read -r response
        
        if [[ ! "$response" =~ ^[Yy] ]]; then
            echo "Operation cancelled."
            exit 0
        fi
        
        for file in $files; do
            # Create backup
            cp "$file" "${file}.bak"
            
            # Replace shebang line
            sed -i.tmp '1s|^#!/bin/bash|#!/usr/bin/env bash|' "$file"
            rm -f "${file}.tmp"
            
            echo "Updated: $file"
            echo "Backup created: ${file}.bak"
        done
        echo "All files have been updated successfully."
        ;;
    *)
        show_usage
        ;;
esac

