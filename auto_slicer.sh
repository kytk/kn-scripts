#!/usr/bin/env bash

# script to generate png files from nifti file
# 13 May 2024 K. Nemoto

### config ###
direction=z
first=5
last=80
##############

for f in "$@"
do
  fname=${f%.nii*}
  mkdir ${fname}_png
  for i in $(seq -w $first $last)
  do
    slicer $fname -u -${direction} -${i} ${fname}_png/${fname}_${i}.png
  done
done

