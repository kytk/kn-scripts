#!/usr/bin/env bash
# A script to normalize FA image using FLIRT and FNIRT implemented in FSL

# Usage: normalize_FA.sh image_file<s>

# K.Nemoto 13 Dec 2023

for f in "$@"
do
  fa=$(imglob $f)
  flirt -ref ${FSLDIR}/data/standard/FMRIB58_FA_1mm 	\
        -in ${fa} -omat ${fa}_affine.mat
  fnirt --config=FA_2_FMRIB58_1mm 			\
        --in=${fa} --aff=${fa}_affine.mat 	\
        --iout=${fa}_fnirted
done

