#!/usr/bin/env bash
# This script uses dcm_another_id.py and inserts another id to DICOM files.

# Preparation
# Please prepare "subjlist"
# subjlist must contain "path to the source directory" and "another id"

# Example
# /path/to/subj1	another_id_subj1
# /path/to/subj2	anotehr_id_subj2

# Usage
# dcm_another_id_batch.sh <subjlist>

if [[ $# -lt 1 ]] ; then
  echo "Please specify the subject list you want to add anotehr ID!"
  echo "Usage: $0 <subjlist>"
  exit 1
fi

# convert CR+LF to CR
tmplist=$(mktemp)
nkf -wLu $1 > tmplist

cat tmplist | while read path id; do dcm_another_id.py $path $id; done

if [[ $? -eq 0 ]] ; then
  echo "Done"
else
  echo "ERROR"
fi

