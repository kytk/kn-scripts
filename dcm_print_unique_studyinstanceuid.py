#!/usr/bin/env python3

import os
import sys
import pydicom
from pydicom.errors import InvalidDicomError

def find_dicom_files(directory):
    """
    Recursively find and list DICOM file paths in the specified directory and its subdirectories.
    """
    dicom_files = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.lower().endswith((".dcm", ".dicom")):
                dicom_files.append(os.path.join(root, file))
    return dicom_files

def extract_unique_study_instance_uids(dicom_files):
    """
    Read StudyInstanceUIDs from a list of DICOM files and return a set of unique values.
    """
    unique_uids = set()
    for dicom_file in dicom_files:
        try:
            dicom_data = pydicom.dcmread(dicom_file)
            unique_uids.add(dicom_data.StudyInstanceUID)
        except InvalidDicomError:
            print(f"Warning: Invalid DICOM file skipped: {dicom_file}")
        except AttributeError:
            print(f"Warning: DICOM file without StudyInstanceUID skipped: {dicom_file}")
    return unique_uids

def main(directory):
    """
    Extract and display unique StudyInstanceUIDs from DICOM files in the specified directory.
    """
    dicom_files = find_dicom_files(directory)
    unique_uids = extract_unique_study_instance_uids(dicom_files)
    print("Unique StudyInstanceUIDs found:")
    for uid in unique_uids:
        print(uid)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <directory_path>")
        sys.exit(1)
    directory = sys.argv[1]
    main(directory)

