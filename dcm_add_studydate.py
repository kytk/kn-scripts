#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# A script to add "Study Date" tag (0008,0020)

# 11 Jul 2023 K. Nemoto
 
import sys, os, time, argparse
import pydicom
 
__version__ = '20230711'
 
__desc__ = '''
Add Study Date 
Please specify two arguments; "DICOM directory" and "Study Date"
'''
__epilog__ = '''
examples:
  dcm_add_studydescrip.py DICOM_DIR StudyDate 
'''

def add_study_date(src_dir, study_date):
    # modify files
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            try:
                src_file = os.path.join(root, file)
                ds = pydicom.dcmread(src_file)
                ds.add_new([0x08,0x20], 'DA', study_date)
                ds.save_as(src_file)
            except:
                pass


if __name__ == '__main__':
    start_time = time.time()
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('DICOM_DIR', help='DICOM directory', nargs=1)
    parser.add_argument('StudyDate', help='Study Date', nargs=1)
 
    err = 0
    try:
        args = parser.parse_args()
        print("Add {} as StudyDate in {}".format(args.StudyDate[0], args.DICOM_DIR[0]))
        add_study_date(args.DICOM_DIR[0], args.StudyDate[0])
        print("execution time: %.2f second." % (time.time() - start_time))
    except Exception as e:
        print("%s: error: %s" % (__file__, str(e)))
        err = 1
 
    sys.exit(err)
