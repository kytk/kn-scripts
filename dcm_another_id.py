#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# A script to add another ID in "Other Patinet Names" tag (0010,1001)

# 09 Jan 2023 K. Nemoto
 
import sys, os, time, argparse
import pydicom
 
__version__ = '1.0 (2023/01/09)'
 
__desc__ = '''
Add another ID in "Other Patient Names"
'''
__epilog__ = '''
examples:
  dcm_another_id.py DICOM_DIR ANOTHER_ID
'''

def add_another_id(src_dir, another_id):
    # modify files
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            try:
                src_file = os.path.join(root, file)
                ds = pydicom.dcmread(src_file)
                ds.add_new([0x10,0x1001], 'PN', str(another_id))
                #print('Add ID: {} in {}'.format(another_id,src_file))
                ds.save_as(src_file)
            except:
                pass


if __name__ == '__main__':
    start_time = time.time()
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('dirs', metavar='DICOM_DIR', help='DICOM directory', nargs=1)
    parser.add_argument('another_id', help='another ID', nargs=1)
 
    err = 0
    try:
        args = parser.parse_args()
        print("add {} as another ID in {}".format(args.another_id, args.dirs[0]))
        add_another_id(args.dirs[0], args.another_id[0])
        print("execution time: %.2f second." % (time.time() - start_time))
    except Exception as e:
        print("%s: error: %s" % (__file__, str(e)))
        err = 1
 
    sys.exit(err)
