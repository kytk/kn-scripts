#!/usr/bin/env python3

import os
import sys
import pydicom

def find_mixed_studyinstanceuid(directory):
    uid_dict = {}
    for root, dirs, files in os.walk(directory):
        for file in files:
            try:
                dicom_file_path = os.path.join(root, file)
                dicom_file = pydicom.dcmread(dicom_file_path, force=True)
                if "StudyInstanceUID" in dicom_file:
                    study_instance_uid = dicom_file.StudyInstanceUID
                    # UIDに基づいてファイルパスを保存
                    if study_instance_uid in uid_dict:
                        uid_dict[study_instance_uid].append((root, file))
                    else:
                        uid_dict[study_instance_uid] = [(root, file)]
            except Exception as e:
                print(f"Error reading {file}: {e}")
    
    # 異なるStudyInstanceUIDが含まれているかチェック
    if len(uid_dict) > 1:
        print("Found files with different StudyInstanceUIDs:")
        for uid, files in uid_dict.items():
            for file_info in files:
                print(f"{os.path.basename(file_info[0])}, {file_info[1]}, {uid}")
    else:
        # すべてのファイルが同じUIDを持つ場合、そのUIDを出力
        unique_uid = next(iter(uid_dict))
        print(f"All files have the same StudyInstanceUID: {unique_uid}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: find_mixed_studyinstanceuid.py DICOM_DIR")
        sys.exit(1)
    
    dicom_dir = sys.argv[1]
    find_mixed_studyinstanceuid(dicom_dir)

