#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# A script to fill the PatientID field with the name of the source directory

# 09 Sep 2023 K. Nemoto

import sys
import os
import time
import argparse
import pydicom

__version__ = '20230909'
__desc__ = '''
Fill the PatientName and PatientID fields with the name of the input directory
'''
__epilog__ = '''
examples:
  dcm_fillid.py DICOM_DIR [DICOM_DIR ...]
'''

def fill_id(src_dir):
    # Modify files in the specified directory
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            try:
                src_file = os.path.join(root, file)
                ds = pydicom.dcmread(src_file)
                pid = src_dir.replace('/', '')
                # Update PatientName and PatientID fields
                ds.PatientName = pid
                ds.PatientID = pid
                ds.save_as(src_file)
            except:
                pass

def main():
    start_time = time.time()
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('dirs', metavar='DICOM_DIR', help='DICOM directory or directories to process.', nargs='+')

    err = 0
    try:
        args = parser.parse_args()
        for dir in args.dirs:
            print(f'Changing the PatientName and PatientID of {dir} to {dir.replace("/", "")}')
            fill_id(dir)
        print(f"Execution time: {time.time() - start_time:.2f} seconds.")
    except Exception as e:
        print(f"{__file__}: error: {str(e)}")
        err = 1

    sys.exit(err)

if __name__ == '__main__':
    main()
