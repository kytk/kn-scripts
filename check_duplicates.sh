#!/usr/bin/env bash

modir=$PWD

[[ -d dup ]] || mkdir dup

for dir in $(ls -F | grep / | grep -v dup)
do 
  echo $dir
  cd $dir
  fdupes .
  cd $modir
done

