#!/usr/bin/env bash

for f in "$@"

do
 if [[ "$f" == *_den_unr_preproc_unbiased.mif ]]; then
    dwi=${f%_den_unr_preproc_unbiased.mif}
    dwi2mask $f ${dwi}_mask.nii.gz
  else
    echo "ERROR: Please specify *den_unr_preproc_unbiased.mif files!"
 fi
done

