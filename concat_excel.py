#!/usr/bin/env python3

import glob
import pandas as pd
import os
import argparse

def list_excel_files(directory: str) -> list:
    """Returns a list of all Excel files in the specified directory."""
    return glob.glob(os.path.join(directory, '*.xlsx'))

def read_excel_files(files: list) -> list:
    """Reads Excel files and returns a list of data frames."""
    data_frames = []
    for file in files:
        try:
            data_frames.append(pd.read_excel(file))
            print(f"Read file: {file}")
        except Exception as e:
            print(f"Failed to read {file}: {e}")
    return data_frames

def concatenate_data_frames(data_frames: list) -> pd.DataFrame:
    """Concatenates multiple data frames into one."""
    return pd.concat(data_frames, ignore_index=True)

def save_to_excel(df: pd.DataFrame, output_file: str) -> None:
    """Saves the data frame to an Excel file."""
    try:
        df.to_excel(output_file, index=False)
        print(f"Saved to {output_file}")
    except Exception as e:
        print(f"Failed to save to {output_file}: {e}")

def main() -> None:
    parser = argparse.ArgumentParser(description="Concatenate Excel files into one.")
    parser.add_argument('-o', '--output', default='total.xlsx', help='Output file name')
    args = parser.parse_args()

    directory = '.'
    output_file = args.output

    files = list_excel_files(directory)
    if not files:
        print("No Excel files found.")
        return

    data_frames = read_excel_files(files)
    if not data_frames:
        print("No data frames to concatenate.")
        return

    concatenated_df = concatenate_data_frames(data_frames)
    save_to_excel(concatenated_df, output_file)

if __name__ == '__main__':
    main()
