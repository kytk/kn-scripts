#!/usr/bin/env bash
#A script which converts various image file formats into the specified dpi TIFF format. (Default dpi=300)
#Usage: image2tiff.sh [dpi] filename
#Wild card can be used.

if [ $# -lt 1 ] ; then
  echo "Please specify the files you want to convert!"
  echo "Usage: $0 [dpi] filename"
  exit 1
fi


for image in "$@" ; do
  if [ -f $image ] ; then
    convert -units PixelsPerInch $image -density 300 `echo $image | sed 's/\..*$/.tiff/'`
  else
    echo "$image: No such file"
  fi
done
