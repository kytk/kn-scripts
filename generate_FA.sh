#!/usr/bin/env bash

for f in "$@"
do
  dtimg=$(imglob $f)
  #Eddy current correction
  echo "eddy current correction of ${dtimg}"
  eddy_correct $dtimg ${dtimg}_ecc 0
  
  #Extract B0 image
  fslroi ${dtimg}_ecc ${dtimg}_b0 0 1
  
  #Generate mask using bet
  echo "Generate mask using bet"
  bet ${dtimg}_b0 ${dtimg}_b0_brain -f 0.3 -g 0 -m
  
  #Calculate FA map using dtifit
  echo "Calculate FA map using dtifit"
  dtifit --data=${dtimg}_ecc                      \
         --out=${dtimg}                           \
         --mask=${dtimg}_b0_brain_mask    \
         --bvecs=${dtimg}.bvec                    \
         --bvals=${dtimg}.bval
done

