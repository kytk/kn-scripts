#!/usr/bin/env bash

# Script to reorganize sequence-oriented DICOM directories to 
# subject-oriented DICOM directories

# Assumption: each subject is stored under the sequence
#.
#├── 3DT1
#│   ├── subj01
#│   ├── subj02
#│   └── subj03
#├── DTI
#│   ├── subj01
#│   ├── subj02
#│   └── subj03
#└── fMRI
#    ├── subj01
#    ├── subj02
#    └── subj03

# Usage: Just run the script

# The result will be the followings;
#subjects/
#├── subj01
#│   ├── subj01_3DT1
#│   ├── subj01_DTI
#│   └── subj01_fMRI
#├── subj02
#│   ├── subj02_3DT1
#│   ├── subj02_DTI
#│   └── subj02_fMRI
#└── subj03
#    ├── subj03_3DT1
#    ├── subj03_DTI
#    └── subj03_fMRI

# 13 Feb 2023 K.Nemoto

set -x

# make a "subject" directory
mkdir subjects

# identify directories and reorganize them
find . -mindepth 2 -type d | while read line
do 
  id=$(echo $line | awk -F/ '{ print $3 }')
  newline=$(echo $line | awk -F/ '{ print $3 "_" $2 }')
  [[ -d subjects/$id ]] || mkdir subjects/$id
  cp -r $line subjects/$id/$newline
done

