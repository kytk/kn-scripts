#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# A script to add "Series Description" tag (0008,103e)
# Part of this script is based on the script provided by Yuya Saito

# 15 May 2024 K. Nemoto

import sys
import os
import time
import argparse
import pydicom
from pydicom.errors import InvalidDicomError

__version__ = '20240515'

__desc__ = '''
Add Series Description
Please specify two arguments; "DICOM directory" and "Series Description"
'''
__epilog__ = '''
examples:
  dcm_add_seriesdescrip.py DICOM_DIR SERIES_DESCRIPTION 
'''

def add_series_descrip(src_dir, series_descrip):
    # modify files
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            src_file = os.path.join(root, file)
            try:
                ds = pydicom.dcmread(src_file)
                ds.add_new((0x0008, 0x103E), 'LO', series_descrip.replace('/', ''))
                ds.save_as(src_file)
                print(f"Updated {src_file}")
            except InvalidDicomError:
                print(f"Skipping non-DICOM file: {src_file}")
            except Exception as e:
                print(f"Error processing {src_file}: {e}")

if __name__ == '__main__':
    start_time = time.time()
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('DICOM_DIR', help='DICOM directory')
    parser.add_argument('SERIES_DESCRIP', help='Series Description')
    
    args = parser.parse_args()
    dicom_dir = args.DICOM_DIR
    series_descrip = args.SERIES_DESCRIP

    try:
        print(f"Adding '{series_descrip.replace('/', '')}' as Series Description in {dicom_dir}")
        add_series_descrip(dicom_dir, series_descrip)
        print("Execution time: {:.2f} seconds.".format(time.time() - start_time))
    except Exception as e:
        print(f"{os.path.basename(__file__)}: error: {e}")
        sys.exit(1)
    
    sys.exit(0)
