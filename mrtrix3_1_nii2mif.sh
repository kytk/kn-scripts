#!/usr/bin/env bash

set -x

for f in "$@"
do
  if [[ "$f" == *nii* ]]; then
    dwi=$(imglob $f)
      mrconvert $f  ${dwi}.mif \
        -fslgrad ${dwi}.bvec ${dwi}.bval \
        -datatype float32
  else
    echo "ERROR: Please specify nifti files!"
 fi
done

