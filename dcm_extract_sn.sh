#!/usr/bin/env bash

tmp0=$(mktemp)
tmp1=$(mktemp)
tmp2=$(mktemp)

cwd=$PWD

for dir in "$@"
do
  id=${dir%/}
  cd $dir
  mkdir temp
  find . -type f > $tmp0
  
  cat $tmp0 | while read line
  do
    isdicom=$(file $line | awk '{ print $2 }')
    if [[ $isdicom == "DICOM" ]]; then 
      cp $line temp
      break
    fi
  done
  
# generate dcmsummary.py
cat << EOF > dcmsummary.py
#!/usr/bin/python3

from pydicom import dcmread
import sys

args = sys.argv

ds = dcmread(args[1])

print(ds)
EOF
  
  serialnumber=$(python3 dcmsummary.py $(find temp -type f) |\
    grep '0018, 1000' | awk '{ print $NF }')
  
  echo -e "${id}\t${serialnumber}" >> $tmp1
  cd $cwd
done

echo -e "ID\tSerial_Number" > $tmp2

cat $tmp2 $tmp1 > Serial_Number.tsv


