#!/usr/bin/env bash

#count number of files in a directory under current directory
for dir in $(find . -type d)
do
    echo -e "$dir \t $(find $dir -type f | wc -l)"
done


