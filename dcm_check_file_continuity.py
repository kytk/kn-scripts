#!/usr/bin/env python3
# coding: utf-8

# Script to check the continuity of DICOM files in the specified directory
# 22 Oct 2023 K. Nemoto

import os
import glob
import pydicom
import sys
import argparse

__desc__ = '''
Check the continuity of DICOM files.
'''
__epilog__ = '''
examples:
  dcm_check_file_continuity.py DICOM_DIR [DICOM_DIR ...]
'''

def is_dicom_file(filepath):
    """Check if the given file is a DICOM file."""
    try:
        pydicom.dcmread(filepath)
        return True
    except:
        return False


def check_continuity(src_dir):
    instance_numbers = []  # initialize a list to store instance numbers
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            src_file = os.path.join(root, file)
            if is_dicom_file(src_file):
                ds = pydicom.dcmread(src_file) 
                instance_numbers.append(ds.InstanceNumber)
    instance_numbers.sort()
    number_of_files = len(instance_numbers)
    first_instance_number = instance_numbers[0]
    last_instance_number = instance_numbers[-1]
    check_continuity = [instance_numbers[x+1] - instance_numbers[x] for x in range(len(instance_numbers) - 1)]
    if set(check_continuity) == {1}:
        print('  continuous')
        print(f'  Number of Files: {number_of_files}')
        print(f'  First Instance Number: {first_instance_number}')
        print(f'  Last Instance Number: {last_instance_number}')
    else:
        print('  WARNING: not continuous -- something is wrong')
        print(f'  Number of Files: {number_of_files}')
        print(f'  First Instance Number: {first_instance_number}')
        print(f'  Last Instance Number: {last_instance_number}')

def main():
    parser = argparse.ArgumentParser(description=__desc__, epilog=__epilog__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('dirs', metavar='DICOM_DIR', help='DICOM directory or directories to process.', nargs='+')

    err = 0
    try:
        args = parser.parse_args()
        for dir in args.dirs:
            print(dir)
            check_continuity(dir)
    except Exception as e:
        print(f"{__file__}: error: {str(e)}")
        err = 1

    sys.exit(err)

if __name__ == "__main__":
    main()

