#!/bin/sh
#A script which converts various image file formats into 300 dpi TIFF format.
#Usage: image2tiff300.sh filename
#Wild card can be used.

if [ $# -lt 1 ] ; then
  echo "Please specify the files you want to convert!"
  echo "Usage: $0 filename"
  exit 1
fi

for image in "$@" ; do
  if [ -f $image ] ; then
    convert -units PixelsPerInch $image -density 300 `echo $image | sed 's/\..*$/.tiff/'`
  else
    echo "$image: No such file"
  fi
done
